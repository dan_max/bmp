#include <cstdlib>
#include <string>
#include <cmath>
#include "BMP.h"

using namespace std;

float expectation(rgb **pix, int w, int h, char r) {
    int sum = 0;
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            if (r == 'r') {
                sum += (int) pix[i][j].r;
            } else if (r == 'g') {
                sum += (int) pix[i][j].g;
            } else {
                sum += (int) pix[i][j].b;
            }
        }
    }
    return (1.0 / (w * h)) * sum;
}

float stdDeviation(rgb **pix, int w, int h, float exp, char r) {
    int sum = 0;
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            if (r == 'r') {
                sum += pow((pix[i][j].r - exp), 2);
            } else if (r == 'b') {
                sum += pow((pix[i][j].b - exp), 2);
            } else {
                sum += pow((pix[i][j].g - exp), 2);
            }
        }
    }
    return sqrt((1.0 / ((w * h) - 1)) * sum);
}

float correlation(rgb **pix, char first, char second, int w, int h) {
    float expF = expectation(pix, w, h, first);
    float expS = expectation(pix, w, h, second);
    rgb** temp = new rgb*[h];
    for (int i = 0; i < h; i++) {
        temp[i] = new rgb[w];
    }
    int f, s;
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            if (first == 'r') {
                f = pix[i][j].r;
            } else if (first == 'b') {
                f = pix[i][j].b;
            } else {
                f = pix[i][j].g;
            }
            if (second == 'r') {
                s = pix[i][j].r;
            } else if (second == 'b') {
                s = pix[i][j].b;
            } else {
                s = pix[i][j].g;
            }
            temp[i][j].r = (f - expF) * (s - expS);
        }
    }
    float devF = stdDeviation(pix, w, h, expF, first);
    float devS = stdDeviation(pix, w, h, expF, second);
    float res = expectation(temp, w, h, 'r') / (devF * devS);
    for (int i = 0; i < h; i++) {
        delete [] temp[i];
    }
    delete [] temp;
    return res;
}

unsigned char clipping(int v) {
    if (v < 0) {
        v = 0;
    } else if (v > 255) {
        v = 255;
    }
    return (unsigned char) v;
}

rgb **toYCC(rgb **pix, int w, int h) {
    rgb** res = new rgb*[h];
    for (int i = 0; i < h; i++) {
        res[i] = new rgb[w];
    }
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            res[i][j].g = clipping(floor(pix[i][j].r * 0.299 + pix[i][j].g * 0.587 + pix[i][j].b * 0.114));
            res[i][j].b = clipping(floor(0.5643 * (pix[i][j].b - pix[i][j].g) + 128));
            res[i][j].r = clipping(floor(0.7132 * (pix[i][j].r - pix[i][j].g) + 128));
        }
    }
    return res;
}

rgb** toRGB(rgb **pix, int w, int h) {
    rgb** res = new rgb*[h];
    for (int i = 0; i < h; i++) {
        res[i] = new rgb[w];
    }
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            res[i][j].g = clipping(floor(pix[i][j].g - 0.714 * (pix[i][j].r - 128) - 0.334 * (pix[i][j].b - 128)));
            res[i][j].r = clipping(floor(pix[i][j].g + 1.402 * (pix[i][j].r - 128)));
            res[i][j].b = clipping(floor(pix[i][j].g + 1.772 * (pix[i][j].b - 128)));
        }
    }
    return res;
}

float psnr(rgb **pix, rgb **newpix, int h, int w, char r) {
    int sum = 0;
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            if (r == 'r') {
                sum += pow(pix[i][j].r - newpix[i][j].r, 2);
            } else if (r == 'b') {
                sum += pow(pix[i][j].b - newpix[i][j].b, 2);
            } else {
                sum += pow(pix[i][j].g - newpix[i][j].g, 2);
            }
        }
    }
    return (float) (10 * log10((w * h * pow(255, 2)) / sum));
}

unsigned char mean(unsigned char *v, int lenght) {
    int sum = 0;
    for (int i = 0; i < lenght; i++) {
        sum += v[i];
    }
    return (unsigned char) floor(sum / lenght);
}

float probability(rgb **pix, int h, int w, int v, char r) {
    int sum = 0;
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            if (r == 'r') {
                if (pix[i][j].r == v) {
                    sum++;
                }
            } else if (r == 'b') {
                if (pix[i][j].b == v) {
                    sum++;
                }
            } else {
                if (pix[i][j].g == v) {
                    sum++;
                }
            }
        }
    }
    return (float) sum / (w * h);
}

float entropy(rgb** pix, int h, int w, char r) {
    float sum = 0;
    float p;
    for (int i = 0; i < 256; i++) {
        p = probability(pix, h, w ,i , r);
        if(p != 0){
            sum += p * log2(p);
        }
    }
    return sum * -1;
}

int main(int argc, char** argv) {
    // 1
    string name = "test.bmp";
    // 2
    BMP in = BMP(name);
    rgb** pix = in.getPixels();
    int w = in.getWidth(), h = in.getHeight();
    // 3
    rgb** temp = new rgb*[h];
    for (int i = 0; i < h; i++) {
        temp[i] = new rgb[w];
    }
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            temp[i][j].r = pix[i][j].r;
            temp[i][j].g = 0;
            temp[i][j].b = 0;
        }
    }
    save(in.getFH(), in.getIH(), temp, "r.bmp");
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            temp[i][j].g = pix[i][j].g;
            temp[i][j].r = 0;
            temp[i][j].b = 0;
        }
    }
    save(in.getFH(), in.getIH(), temp, "g.bmp");
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            temp[i][j].b = pix[i][j].b;
            temp[i][j].g = 0;
            temp[i][j].r = 0;
        }
    }
    save(in.getFH(), in.getIH(), temp, "b.bmp");

    for (int i = 0; i < h; i++) {
        delete [] temp[i];
    }
    delete [] temp;
    // 4 a
    cout << "corr r, g = " << correlation(pix, 'r', 'g', w, h) << endl;
    cout << "corr r, b = " << correlation(pix, 'r', 'b', w, h) << endl;
    cout << "corr b, g = " << correlation(pix, 'b', 'g', w, h) << endl;
    // 5
    rgb **ycc = toYCC(pix, w, h);
    // 6
    temp = new rgb*[h];
    for (int i = 0; i < h; i++) {
        temp[i] = new rgb[w];
    }
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            temp[i][j].r = ycc[i][j].r;
            temp[i][j].g = ycc[i][j].r;
            temp[i][j].b = ycc[i][j].r;
        }
    }
    save(in.getFH(), in.getIH(), temp, "cr.bmp");
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            temp[i][j].r = ycc[i][j].g;
            temp[i][j].g = ycc[i][j].g;
            temp[i][j].b = ycc[i][j].g;
        }
    }
    save(in.getFH(), in.getIH(), temp, "y.bmp");
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            temp[i][j].r = ycc[i][j].b;
            temp[i][j].g = ycc[i][j].b;
            temp[i][j].b = ycc[i][j].b;
        }
    }
    save(in.getFH(), in.getIH(), temp, "cb.bmp");
    for (int i = 0; i < h; i++) {
        delete [] temp[i];
    }
    delete [] temp;
    // 7
    temp = toRGB(ycc, w, h);
    save(in.getFH(), in.getIH(), temp, "restored.bmp");
    cout << "psnr r = " << psnr(pix, temp, h, w, 'r') << endl;
    cout << "psnr g = " << psnr(pix, temp, h, w, 'g') << endl;
    cout << "psnr b = " << psnr(pix, temp, h, w, 'b') << endl;
    for (int i = 0; i < h; i++) {
        delete [] temp[i];
    }
    delete [] temp;
    // 8 a
    rgb** yccc = new rgb*[h / 2];
    for (int i = 0; i < h / 2; i++) {
        yccc[i] = new rgb[w / 2];
    }
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            yccc[i / 2][j / 2].r = ycc[i][j].r;
            yccc[i / 2][j / 2].b = ycc[i][j].b;
        }
    }
    // 9 a
    temp = new rgb*[h];
    for (int i = 0; i < h; i++) {
        temp[i] = new rgb[w];
    }
    for (int i = 0; i < h - 1; i++) {
        for (int j = 0; j < w - 1; j++) {
            temp[i][j].r = yccc[i / 2][j / 2].r;
            temp[i + 1][j].r = yccc[i / 2][j / 2].r;
            temp[i][j + 1].r = yccc[i / 2][j / 2].r;
            temp[i + 1][j + 1].r = yccc[i / 2][j / 2].r;
            temp[i][j].g = ycc[i][j].g;
            temp[i + 1][j].g = ycc[i + 1][j].g;
            temp[i][j + 1].g = ycc[i][j + 1].g;
            temp[i + 1][j + 1].g = ycc[i + 1][j + 1].g;
            temp[i][j].b = yccc[i / 2][j / 2].b;
            temp[i + 1][j].b = yccc[i / 2][j / 2].b;
            temp[i][j + 1].b = yccc[i / 2][j / 2].b;
            temp[i + 1][j + 1].b = yccc[i / 2][j / 2].b;
        }
    }
    for (int i = 0; i < h / 2; i++) {
        delete [] yccc[i];
    }
    delete [] yccc;

    rgb **restored = toRGB(temp, w, h);
    save(in.getFH(), in.getIH(), restored, "2a.bmp");
    // 10 a
    cout << "compress 2x a" << endl;
    cout << "psnr r = " << psnr(pix, restored, h, w, 'r') << endl;
    cout << "psnr g = " << psnr(pix, restored, h, w, 'g') << endl;
    cout << "psnr b = " << psnr(pix, restored, h, w, 'b') << endl;
    cout << "psnr cr = " << psnr(ycc, temp, h, w, 'r') << endl;
    cout << "psnr cb = " << psnr(ycc, temp, h, w, 'b') << endl;

    for (int i = 0; i < h; i++) {
        delete [] temp[i];
    }
    delete [] temp;
    // 8 b
    yccc = new rgb*[h / 2];
    for (int i = 0; i < h / 2; i++) {
        yccc[i] = new rgb[w / 2];
    }
    unsigned char *arr = new unsigned char[4];
    for (int i = 0; i < h / 2; i++) {
        for (int j = 0; j < w / 2; j++) {
            arr[0] = ycc[i * 2][j * 2].r;
            arr[1] = ycc[i * 2 + 1][j * 2].r;
            arr[2] = ycc[i * 2][j * 2 + 1].r;
            arr[3] = ycc[i * 2 + 1][j * 2 + 1].r;
            yccc[i][j].r = mean(arr, 4);
            arr[0] = ycc[i * 2][j * 2].b;
            arr[1] = ycc[i * 2 + 1][j * 2].b;
            arr[2] = ycc[i * 2][j * 2 + 1].b;
            arr[3] = ycc[i * 2 + 1][j * 2 + 1].b;
            yccc[i][j].b = mean(arr, 4);
        }
    }
    delete [] arr;
    // 9 b
    temp = new rgb*[h];
    for (int i = 0; i < h; i++) {
        temp[i] = new rgb[w];
    }
    for (int i = 0; i < h - 1; i++) {
        for (int j = 0; j < w - 1; j++) {
            temp[i][j].r = yccc[i / 2][j / 2].r;
            temp[i + 1][j].r = yccc[i / 2][j / 2].r;
            temp[i][j + 1].r = yccc[i / 2][j / 2].r;
            temp[i + 1][j + 1].r = yccc[i / 2][j / 2].r;
            temp[i][j].g = ycc[i][j].g;
            temp[i + 1][j].g = ycc[i + 1][j].g;
            temp[i][j + 1].g = ycc[i][j + 1].g;
            temp[i + 1][j + 1].g = ycc[i + 1][j + 1].g;
            temp[i][j].b = yccc[i / 2][j / 2].b;
            temp[i + 1][j].b = yccc[i / 2][j / 2].b;
            temp[i][j + 1].b = yccc[i / 2][j / 2].b;
            temp[i + 1][j + 1].b = yccc[i / 2][j / 2].b;
        }
    }
    restored = toRGB(temp, w, h);
    save(in.getFH(), in.getIH(), restored, "2b.bmp");
    // 10 b
    cout << "compress 2x b" << endl;
    cout << "psnr r = " << psnr(pix, restored, h, w, 'r') << endl;
    cout << "psnr g = " << psnr(pix, restored, h, w, 'g') << endl;
    cout << "psnr b = " << psnr(pix, restored, h, w, 'b') << endl;
    cout << "psnr cr = " << psnr(ycc, temp, h, w, 'r') << endl;
    cout << "psnr cb = " << psnr(ycc, temp, h, w, 'b') << endl;
    for (int i = 0; i < h; i++) {
        delete [] temp[i];
    }
    delete [] temp;
    // 11  
    // 8 a
    yccc = new rgb*[h / 4];
    for (int i = 0; i < h / 4; i++) {
        yccc[i] = new rgb[w / 4];
    }
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            yccc[i / 4][j / 4].r = ycc[i][j].r;
            yccc[i / 4][j / 4].b = ycc[i][j].b;
        }
    }
    // 9 a
    temp = new rgb*[h];
    for (int i = 0; i < h; i++) {
        temp[i] = new rgb[w];
    }
    for (int i = 0; i < h - 3; i++) {
        for (int j = 0; j < w - 3; j++) {
            temp[i][j].r = yccc[i / 4][j / 4].r;
            temp[i][j + 1].r = yccc[i / 4][j / 4].r;
            temp[i][j + 2].r = yccc[i / 4][j / 4].r;
            temp[i][j + 3].r = yccc[i / 4][j / 4].r;
            temp[i + 1][j].r = yccc[i / 4][j / 4].r;
            temp[i + 1][j + 1].r = yccc[i / 4][j / 4].r;
            temp[i + 1][j + 2].r = yccc[i / 4][j / 4].r;
            temp[i + 1][j + 3].r = yccc[i / 4][j / 4].r;
            temp[i + 2][j].r = yccc[i / 4][j / 4].r;
            temp[i + 2][j + 1].r = yccc[i / 4][j / 4].r;
            temp[i + 2][j + 2].r = yccc[i / 4][j / 4].r;
            temp[i + 2][j + 3].r = yccc[i / 4][j / 4].r;
            temp[i + 3][j].r = yccc[i / 4][j / 4].r;
            temp[i + 3][j + 1].r = yccc[i / 4][j / 4].r;
            temp[i + 3][j + 2].r = yccc[i / 4][j / 4].r;
            temp[i + 3][j + 3].r = yccc[i / 4][j / 4].r;
            temp[i][j].g = ycc[i][j].g;
            temp[i][j + 1].g = ycc[i][j + 1].g;
            temp[i][j + 2].g = ycc[i][j + 2].g;
            temp[i][j + 3].g = ycc[i][j + 3].g;
            temp[i + 1][j].g = ycc[i + 1][j].g;
            temp[i + 1][j + 1].g = ycc[i + 1][j + 1].g;
            temp[i + 1][j + 2].g = ycc[i + 1][j + 2].g;
            temp[i + 1][j + 3].g = ycc[i + 1][j + 3].g;
            temp[i + 2][j].g = ycc[i + 2][j].g;
            temp[i + 2][j + 1].g = ycc[i + 2][j + 1].g;
            temp[i + 2][j + 2].g = ycc[i + 2][j + 2].g;
            temp[i + 2][j + 3].g = ycc[i + 2][j + 3].g;
            temp[i + 3][j].g = ycc[i + 3][j].g;
            temp[i + 3][j + 1].g = ycc[i + 3][j + 1].g;
            temp[i + 3][j + 2].g = ycc[i + 3][j + 2].g;
            temp[i + 3][j + 3].g = ycc[i + 3][j + 3].g;
            temp[i][j].b = yccc[i / 4][j / 4].b;
            temp[i][j + 1].b = yccc[i / 4][j / 4].b;
            temp[i][j + 2].b = yccc[i / 4][j / 4].b;
            temp[i][j + 3].b = yccc[i / 4][j / 4].b;
            temp[i + 1][j].b = yccc[i / 4][j / 4].b;
            temp[i + 1][j + 1].b = yccc[i / 4][j / 4].b;
            temp[i + 1][j + 2].b = yccc[i / 4][j / 4].b;
            temp[i + 1][j + 3].b = yccc[i / 4][j / 4].b;
            temp[i + 2][j].b = yccc[i / 4][j / 4].b;
            temp[i + 2][j + 1].b = yccc[i / 4][j / 4].b;
            temp[i + 2][j + 2].b = yccc[i / 4][j / 4].b;
            temp[i + 2][j + 3].b = yccc[i / 4][j / 4].b;
            temp[i + 3][j].b = yccc[i / 4][j / 4].b;
            temp[i + 3][j + 1].b = yccc[i / 4][j / 4].b;
            temp[i + 3][j + 2].b = yccc[i / 4][j / 4].b;
            temp[i + 3][j + 3].b = yccc[i / 4][j / 4].b;
        }
    }
    for (int i = 0; i < h / 4; i++) {
        delete [] yccc[i];
    }
    delete [] yccc;
    restored = toRGB(temp, w, h);
    save(in.getFH(), in.getIH(), restored, "4a.bmp");
    // 10 a
    cout << "compress 4x a" << endl;
    cout << "psnr r = " << psnr(pix, restored, h, w, 'r') << endl;
    cout << "psnr g = " << psnr(pix, restored, h, w, 'g') << endl;
    cout << "psnr b = " << psnr(pix, restored, h, w, 'b') << endl;
    cout << "psnr cr = " << psnr(ycc, temp, h, w, 'r') << endl;
    cout << "psnr cb = " << psnr(ycc, temp, h, w, 'b') << endl;
    for (int i = 0; i < h; i++) {
        delete [] temp[i];
    }
    // 8 b
    yccc = new rgb*[h / 4];
    for (int i = 0; i < h / 4; i++) {
        yccc[i] = new rgb[w / 4];
    }
    arr = new unsigned char[16];
    for (int i = 0; i < h / 4; i++) {
        for (int j = 0; j < w / 4; j++) {
            arr[0] = ycc[i * 4][j * 4].r;
            arr[1] = ycc[i * 4][j * 4 + 1].r;
            arr[2] = ycc[i * 4][j * 4 + 2].r;
            arr[3] = ycc[i * 4][j * 4 + 3].r;
            arr[4] = ycc[i * 4 + 1][j * 4].r;
            arr[5] = ycc[i * 4 + 1][j * 4 + 1].r;
            arr[6] = ycc[i * 4 + 1][j * 4 + 2].r;
            arr[7] = ycc[i * 4 + 1][j * 4 + 3].r;
            arr[8] = ycc[i * 4 + 2][j * 4].r;
            arr[9] = ycc[i * 4 + 2][j * 4 + 1].r;
            arr[10] = ycc[i * 4 + 2][j * 4 + 2].r;
            arr[11] = ycc[i * 4 + 2][j * 4 + 3].r;
            arr[12] = ycc[i * 4][j * 4].r;
            arr[13] = ycc[i * 4 + 3][j * 4 + 1].r;
            arr[14] = ycc[i * 4 + 3][j * 4 + 2].r;
            arr[15] = ycc[i * 4 + 3][j * 4 + 3].r;
            yccc[i][j].r = mean(arr, 16);
            arr[0] = ycc[i * 4][j * 4].b;
            arr[1] = ycc[i * 4][j * 4 + 1].b;
            arr[2] = ycc[i * 4][j * 4 + 2].b;
            arr[3] = ycc[i * 4][j * 4 + 3].b;
            arr[4] = ycc[i * 4 + 1][j * 4].b;
            arr[5] = ycc[i * 4 + 1][j * 4 + 1].b;
            arr[6] = ycc[i * 4 + 1][j * 4 + 2].b;
            arr[7] = ycc[i * 4 + 1][j * 4 + 3].b;
            arr[8] = ycc[i * 4 + 2][j * 4].b;
            arr[9] = ycc[i * 4 + 2][j * 4 + 1].b;
            arr[10] = ycc[i * 4 + 2][j * 4 + 2].b;
            arr[11] = ycc[i * 4 + 2][j * 4 + 3].b;
            arr[12] = ycc[i * 4][j * 4].b;
            arr[13] = ycc[i * 4 + 3][j * 4 + 1].b;
            arr[14] = ycc[i * 4 + 3][j * 4 + 2].b;
            arr[15] = ycc[i * 4 + 3][j * 4 + 3].b;
            yccc[i][j].b = mean(arr, 16);
        }
    }
    delete [] arr;
    // 9 b
    temp = new rgb*[h];
    for (int i = 0; i < h; i++) {
        temp[i] = new rgb[w];
    }
    for (int i = 0; i < h - 3; i++) {
        for (int j = 0; j < w - 3; j++) {
            temp[i][j].r = yccc[i / 4][j / 4].r;
            temp[i][j + 1].r = yccc[i / 4][j / 4].r;
            temp[i][j + 2].r = yccc[i / 4][j / 4].r;
            temp[i][j + 3].r = yccc[i / 4][j / 4].r;
            temp[i + 1][j].r = yccc[i / 4][j / 4].r;
            temp[i + 1][j + 1].r = yccc[i / 4][j / 4].r;
            temp[i + 1][j + 2].r = yccc[i / 4][j / 4].r;
            temp[i + 1][j + 3].r = yccc[i / 4][j / 4].r;
            temp[i + 2][j].r = yccc[i / 4][j / 4].r;
            temp[i + 2][j + 1].r = yccc[i / 4][j / 4].r;
            temp[i + 2][j + 2].r = yccc[i / 4][j / 4].r;
            temp[i + 2][j + 3].r = yccc[i / 4][j / 4].r;
            temp[i + 3][j].r = yccc[i / 4][j / 4].r;
            temp[i + 3][j + 1].r = yccc[i / 4][j / 4].r;
            temp[i + 3][j + 2].r = yccc[i / 4][j / 4].r;
            temp[i + 3][j + 3].r = yccc[i / 4][j / 4].r;
            temp[i][j].g = ycc[i][j].g;
            temp[i][j + 1].g = ycc[i][j + 1].g;
            temp[i][j + 2].g = ycc[i][j + 2].g;
            temp[i][j + 3].g = ycc[i][j + 3].g;
            temp[i + 1][j].g = ycc[i + 1][j].g;
            temp[i + 1][j + 1].g = ycc[i + 1][j + 1].g;
            temp[i + 1][j + 2].g = ycc[i + 1][j + 2].g;
            temp[i + 1][j + 3].g = ycc[i + 1][j + 3].g;
            temp[i + 2][j].g = ycc[i + 2][j].g;
            temp[i + 2][j + 1].g = ycc[i + 2][j + 1].g;
            temp[i + 2][j + 2].g = ycc[i + 2][j + 2].g;
            temp[i + 2][j + 3].g = ycc[i + 2][j + 3].g;
            temp[i + 3][j].g = ycc[i + 3][j].g;
            temp[i + 3][j + 1].g = ycc[i + 3][j + 1].g;
            temp[i + 3][j + 2].g = ycc[i + 3][j + 2].g;
            temp[i + 3][j + 3].g = ycc[i + 3][j + 3].g;
            temp[i][j].b = yccc[i / 4][j / 4].b;
            temp[i][j + 1].b = yccc[i / 4][j / 4].b;
            temp[i][j + 2].b = yccc[i / 4][j / 4].b;
            temp[i][j + 3].b = yccc[i / 4][j / 4].b;
            temp[i + 1][j].b = yccc[i / 4][j / 4].b;
            temp[i + 1][j + 1].b = yccc[i / 4][j / 4].b;
            temp[i + 1][j + 2].b = yccc[i / 4][j / 4].b;
            temp[i + 1][j + 3].b = yccc[i / 4][j / 4].b;
            temp[i + 2][j].b = yccc[i / 4][j / 4].b;
            temp[i + 2][j + 1].b = yccc[i / 4][j / 4].b;
            temp[i + 2][j + 2].b = yccc[i / 4][j / 4].b;
            temp[i + 2][j + 3].b = yccc[i / 4][j / 4].b;
            temp[i + 3][j].b = yccc[i / 4][j / 4].b;
            temp[i + 3][j + 1].b = yccc[i / 4][j / 4].b;
            temp[i + 3][j + 2].b = yccc[i / 4][j / 4].b;
            temp[i + 3][j + 3].b = yccc[i / 4][j / 4].b;
        }
    }
    restored = toRGB(temp, w, h);
    save(in.getFH(), in.getIH(), restored, "4b.bmp");
    // 10 b
    cout << "compress 4x b" << endl;
    cout << "psnr r = " << psnr(pix, restored, h, w, 'r') << endl;
    cout << "psnr g = " << psnr(pix, restored, h, w, 'g') << endl;
    cout << "psnr b = " << psnr(pix, restored, h, w, 'b') << endl;
    cout << "psnr cr = " << psnr(ycc, temp, h, w, 'r') << endl;
    cout << "psnr cb = " << psnr(ycc, temp, h, w, 'b') << endl;
    for (int i = 0; i < h; i++) {
        delete [] temp[i];
    }
    delete [] temp;
    // 12 grapihxs ??

    // 13
    cout << "entropy r = " << entropy(pix, h, w, 'r') << endl;
    cout << "entropy g = " << entropy(pix, h, w, 'g') << endl;
    cout << "entropy b = " << entropy(pix, h, w, 'b') << endl;
    cout << "entropy y = " << entropy(ycc, h, w, 'g') << endl;
    cout << "entropy Cr = " << entropy(ycc, h, w, 'r') << endl;
    cout << "entropy Cb = " << entropy(ycc, h, w, 'b') << endl;
    //p14
    return 0;
}


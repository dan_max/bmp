/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BMP.cpp
 * Author: danmax
 * 
 * Created on 21 февраля 2019 г., 20:41
 */

#include "BMP.h"

BMP::BMP(string name) {
    h = new bmpFH;
    i = new bmpIH;
    pix = load(h, i, name);
}

BMP::~BMP() {
    if (pix != nullptr) {
        for (int j = 0; j < i->biHeight; j++) {
            delete [] pix[j];
        }
        delete [] pix;
    }
    delete i;
    delete h;
}

rgb** BMP::load(bmpFH* bf, bmpIH* bi, string name) {
    ifstream f(name, ios_base::in | ios_base::binary);
    if (!f.is_open()) {
        cout << "open file error" << endl;
        return nullptr;
    }
    f.read((char*) (&bf->bfType), sizeof (bf->bfType));
    f.read((char*) (&bf->bfSize), sizeof (bf->bfSize));
    f.read((char*) (&bf->bfReserved), sizeof (bf->bfReserved));
    f.read((char*) (&bf->BfOffBits), sizeof (bf->BfOffBits));
    f.read((char*) (&bi->biSize), sizeof (bi->biSize));
    f.read((char*) (&bi->biWidth), sizeof (bi->biWidth));
    f.read((char*) (&bi->biHeight), sizeof (bi->biHeight));
    f.read((char*) (&bi->biPlanes), sizeof (bi->biPlanes));
    f.read((char*) (&bi->biBitCount), sizeof (bi->biBitCount));
    f.read((char*) (&bi->biCompression), sizeof (bi->biCompression));
    f.read((char*) (&bi->biSizeImage), sizeof (bi->biSizeImage));
    f.read((char*) (&bi->biXPelsPerMeter), sizeof (bi->biXPelsPerMeter));
    f.read((char*) (&bi->biYPelsPerMeter), sizeof (bi->biYPelsPerMeter));
    f.read((char*) (&bi->biClrUsed), sizeof (bi->biClrUsed));
    f.read((char*) (&bi->biClrImportant), sizeof (bi->biClrImportant));
    rgb** pixels = new rgb*[bi->biHeight];
    for (int i = 0; i < bi->biHeight; i++) {
        pixels[i] = new rgb[bi->biWidth];
        for (int j = 0; j < bi->biWidth; j++) {
            f.read((char*) (&(pixels[i][j].b)), sizeof (pixels[i][j].b));
            f.read((char*) (&(pixels[i][j].g)), sizeof (pixels[i][j].g));
            f.read((char*) (&((pixels[i][j].r))), sizeof (pixels[i][j].r));
        }
        int flag = (bi->biWidth * bi->biBitCount / 8) % 4;
        while (flag != 0) {
            flag = (flag + 1) % 4;
            unsigned char x = 0;
            f.read((char*) (&x), sizeof (x));
        }
    }
    return pixels;
}

void save(bmpFH* bf, bmpIH* bi, rgb** pix, string name) {
    ofstream f(name, ios_base::out | ios_base::binary);
    f.write((char*) (&bf->bfType), sizeof (bf->bfType));
    f.write((char*) (&bf->bfSize), sizeof (bf->bfSize));
    f.write((char*) (&bf->bfReserved), sizeof (bf->bfReserved));
    f.write((char*) (&bf->BfOffBits), sizeof (bf->BfOffBits));
    f.write((char*) (&bi->biSize), sizeof (bi->biSize));
    f.write((char*) (&bi->biWidth), sizeof (bi->biWidth));
    f.write((char*) (&bi->biHeight), sizeof (bi->biHeight));
    f.write((char*) (&bi->biPlanes), sizeof (bi->biPlanes));
    f.write((char*) (&bi->biBitCount), sizeof (bi->biBitCount));
    f.write((char*) (&bi->biCompression), sizeof (bi->biCompression));
    f.write((char*) (&bi->biSizeImage), sizeof (bi->biSizeImage));
    f.write((char*) (&bi->biXPelsPerMeter), sizeof (bi->biXPelsPerMeter));
    f.write((char*) (&bi->biYPelsPerMeter), sizeof (bi->biYPelsPerMeter));
    f.write((char*) (&bi->biClrUsed), sizeof (bi->biClrUsed));
    f.write((char*) (&bi->biClrImportant), sizeof (bi->biClrImportant));
    for (int i = 0; i < bi->biHeight; i++) {
        for (int j = 0; j < bi->biWidth; j++) {
            f.write((char*) (&(pix[i][j].b)), sizeof (pix[i][j].b));
            f.write((char*) (&(pix[i][j].g)), sizeof (pix[i][j].g));
            f.write((char*) (&((pix[i][j].r))), sizeof (pix[i][j].r));
        }
        int flag = (bi->biWidth * bi->biBitCount / 8) % 4;
        while (flag != 0) {
            flag = (flag + 1) % 4;
            unsigned char x = 0;
            f.write((char*) (&x), sizeof (x));
        }
    }
}

rgb** BMP::getPixels() {
    return pix;
}

int BMP::getHeight() {
    return i->biHeight;
}

int BMP::getWidth() {
    return i->biWidth;
}

BMP::BMP() {

}

bmpFH* BMP::getFH() {
    return h;
}

bmpIH* BMP::getIH() {
    return i;
}

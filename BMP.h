/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BMP.h
 * Author: danmax
 *
 * Created on 21 февраля 2019 г., 20:41
 */

#ifndef BMP_H
#define BMP_H

#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>

using namespace std;

typedef struct BITMAPFILEHEADER {
    unsigned char bfType[2];
    unsigned int bfSize;
    unsigned int bfReserved;
    unsigned int BfOffBits;
} bmpFH;

typedef struct BITMAPINFOHEADER {
    unsigned int biSize;
    unsigned int biWidth;
    unsigned int biHeight;
    unsigned short biPlanes;
    unsigned short biBitCount;
    unsigned int biCompression;
    unsigned int biSizeImage;
    unsigned int biXPelsPerMeter;
    unsigned int biYPelsPerMeter;
    unsigned int biClrUsed;
    unsigned int biClrImportant;
} bmpIH;

typedef struct RGBQUADS {
    unsigned char b;
    unsigned char g;
    unsigned char r;
} rgb;

void save(bmpFH* bf, bmpIH* bi, rgb** pix, string name);

class BMP {
public:
    BMP();
    BMP(string name);
    virtual ~BMP();
    rgb** getPixels();
    int getHeight();
    int getWidth();
    bmpFH* getFH();
    bmpIH* getIH();
private:
    bmpFH *h = nullptr;
    bmpIH *i = nullptr;
    rgb **pix = nullptr;
    rgb** load(bmpFH* bf, bmpIH* bi, string name);
};

#endif /* BMP_H */

